cd~
Explained: it is shortcut to swiftly navigate to user's home directory in a command-line interface
cd portfolio
Explained: used to change the current directory to a directory named "portfolio" and navigates from current location to specified directory
mkdir week3
Explained: create a new directory named "week3" in the current location.
mkdir week3/greeting
Explained: creates a "greeting" directory within "week3"
cd week3/greeting
Explained: navigates to the "greeting" directory within "week3"
git branch greeting
Explained: creates a new branch in a Git repository named "greeting."
git switch greeting
Explained: to switch to an existing branch named "greeting" in a Git repository
greeting.c
 #include <stdio.h>
         int greet(void) {
           printf("Hello world!\n");
return 0; }
Explained: generates a file named greeting.c containing a basic C program that prints "Hello world!" to the console when executed.
gcc -Wall -pedantic -c greeting.c -o greeting.o
Explained: compiles greeting.c into greeting.o with strict error checking and warnings.
test_result.c
 #include <assert.h>
       #include "greeting.h"
       int main(void) {
         assert(0==greet());
         return 0;
}
Explained: includes headers, defines a main() function using assertions to verify that greet() returns 0.
greeting.h 
  int greet(void); 
Explained: declares the function greet() without specifying its internal implementation.
echogreeting.o>>~/portfolio/.gitignore 
Explained: Appends "greeting.o" to ~/portfolio/.gitignore
echo libgreet.a >> ~/portfolio/.gitignore 
Explained: Appends "libgreet.a" to ~/portfolio/.gitignore
ar rv libgreet.a greeting.o
Explained: ar rv libgreet.a greeting.o archives greeting.o into libgreet.a
gcc test_result.c -o test1 -L. -lgreet -I.
Explained: compiles test_result.c into test1, linking with libgreet from current directory and including current directory headers during compilation.
./test1
Explained: runs the test1 executable from the current directory.
gitadd-A
Explained: git add -A stages all changes in the repository.
git commit -m “greeting library and greeting test program”
Explained: commits changes with a descriptive message.
gitpush
Explained: git push updates the remote repository with local changes.
gitswitchmaster
Explained: git switch master switches to the "master" branch.
gitbranchvectors
Explained: creates a branch named "vectors."
gitswitchvectors
Explained: changes to the "vectors" branch
cd~/portfolio/week3
Explained: navigates to the "week3" directory in the "portfolio" directory.
mkdirvectors
Explained: makes a directory named "vectors"
cdvectors
Explained: navigates to the "vectors" directory.
vector.h
#define SIZ 3
int add_vectors(int x[], int y[], int z[]); 
Explained: vector.h defines a constant SIZ as 3 and declares function add_vectors to add two arrays of integers and store result in a third array
test_vector_add.c
      #include <assert.h>
      #include "vector.h"
      int main(void) {
int xvec[SIZ]={1,2,3};
int yvec[SIZ]={5,0,2};
int zvec[SIZ];
add_vectors(xvec,yvec,zvec);
assert(6==zvec[0]);
assert(2==zvec[1]);
assert(5==zvec[2]);
Explained: test_vector_add.c includes headers, initializes arrays, adds vectors, and asserts expected values.
gcc -Wall -pedantic -c vector.c -o vector.o
Explained: it compiles vector.c into vector.o with strict error checking.
ar rv libvector.avector.o
Explained:ar rv libvector.a vector.o - Archives vector.o into libvector.a
gcc test_vector_add.c -o test_vector_add1 -L. -lvector -I. ./test_vector_add1
Explained: it compiles, links, and executes the program test_vector_add1.
git add -A
Explained:stages all changes in the repository
git commit -m “code to add two vectors of fixed size”
Explained: commits changes with a specific message.
git push
Explained: updates the remote repository with local changes.
test_vector_dot_product.c
#include <assert.h>
#include "vector.h"
int main(void) {
int xvec[SIZ]={1,2,3};
int yvec[SIZ]={5,0,2};
int result;
result=dot_product(xvec,yvec);
assert(11==result);
return 0; }
Explained: includes headers, initializes arrays, computes dot product, and asserts the expected value.
